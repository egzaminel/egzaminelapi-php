<?php
class DB_SubjectGroups {
    private $conn;

    function __construct() {
        require_once 'DB_Connect.php';
        $db = new DB_connect();
        $this->conn = $db->connect();
    }

    function __destruct() {
    }

    public function getSubjectGroupsWithEvents($subject_id) {
        $stmt_groups = $this->conn->prepare
        ('SELECT subjects_groups.id, subjects_groups.subject_id,
                  subjects_groups.date, subjects_groups.perioid, subjects_groups.place, subjects_groups.teacher,
                  subjects_groups.description, subjects_groups.last_update
                  FROM subjects_groups
                  WHERE subjects_groups.subject_id = ?
                '
        );
        $stmt_groups->bind_param("i", $subject_id);
        $stmt_groups->execute();
        $data_groups = $stmt_groups->get_result();

        $res_arr = array();
        while ($row = $data_groups->fetch_assoc()) {
            $row['events'] = $this->getSubjectGroupsEvents($row['id']);
            $res_arr[] = $row;
        }
        return $res_arr;
    }

    public function getSubjectGroupsEvents($subject_group_id) {
        $stmt = $this->conn->prepare
        ('SELECT *
                  FROM events_subjects_groups
                  WHERE events_subjects_groups.parent_id = ?'
        );
        $stmt->bind_param("i", $subject_group_id);
        $stmt->execute();
        $data = $stmt->get_result();
        $res_arr = array();
        while ($row = $data->fetch_assoc()) {
            $res_arr[] = $row;
        }
        return $res_arr;
    }

}