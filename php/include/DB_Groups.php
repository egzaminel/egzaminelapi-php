<?php
class DB_Groups {
    private $conn;

    private $subject_controller;

    function __construct() {
        require_once 'DB_Connect.php';
        require_once 'DB_Subjects.php';

        $db = new DB_connect();
        $this->subject_controller = new DB_Subjects();
        $this->conn = $db->connect();
    }

    function __destruct() {
    }

    public function getAllData($user_id) {
        $stmt_groups = $this->conn->prepare
        ('SELECT groups.id, groups.name, groups.description, groups.last_update
                  FROM groups
                  JOIN groups_members ON groups_members.group_id = groups.id
                  WHERE groups_members.user_id = ?
                '
        );
        $stmt_groups->bind_param("i", $user_id);
        $stmt_groups->execute();
        $data_groups = $stmt_groups->get_result();

        $res_arr = array();
        while ($row = $data_groups->fetch_assoc()) {
            $row['events'] = $this->getGroupEvents($row['id']);
            $row['subjects'] = $this->subject_controller->getSubjectsWithEvents($row['id']);
            $res_arr[] = $row;
        }

        return $res_arr;
    }

    public function getGroupData($group_id) {
        $stmt_groups = $this->conn->prepare
        ('SELECT groups.id, groups.name, groups.description, groups.last_update
                  FROM groups
                  WHERE groups.id = ?
                '
        );
        $stmt_groups->bind_param("i", $group_id);
        $stmt_groups->execute();
        $data_groups = $stmt_groups->get_result();

        $res_arr = array();
        while ($row = $data_groups->fetch_assoc()) {
            $row['events'] = $this->getGroupEvents($row['id']);
            $row['subjects'] = $this->subject_controller->getSubjectsWithEvents($row['id']);
            $res_arr[] = $row;
        }

        return $res_arr;
    }

    public function getGroupEvents($group_id) {
        $stmt = $this->conn->prepare
        ('SELECT *
                  FROM events_groups
                  WHERE events_groups.parent_id = ?'
        );
        $stmt->bind_param("i", $group_id);
        $stmt->execute();
        $data = $stmt->get_result();
        $res_arr = array();
        while ($row = $data->fetch_assoc()) {
            $res_arr[] = $row;
        }
        return $res_arr;
    }
}