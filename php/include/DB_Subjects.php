<?php
class DB_Subjects {
    private $conn;
    private $subject_groups_controller;

    function __construct() {
        require_once 'DB_Connect.php';
        require_once 'DB_SubjectGroups.php';
        $db = new DB_connect();
        $this->subject_groups_controller = new DB_SubjectGroups();
        $this->conn = $db->connect();
    }

    function __destruct() {
    }

    public function getSubjectsWithEvents($group_id) {
        $stmt_groups = $this->conn->prepare
        ('SELECT subjects.id, subjects.group_id, subjects.name, subjects.description, subjects.last_update
                  FROM subjects
                  WHERE subjects.group_id = ?
                '
        );
        $stmt_groups->bind_param("i", $group_id);
        $stmt_groups->execute();
        $data_groups = $stmt_groups->get_result();

        $res_arr = array();
        while ($row = $data_groups->fetch_assoc()) {
            $row['events'] = $this->getSubjectEvents($row['id']);
            $row['subject_groups'] = $this->subject_groups_controller->getSubjectGroupsWithEvents($row['id']);
            $res_arr[] = $row;
        }
        return $res_arr;
    }

    public function getSubjectEvents($subject_id) {
        $stmt = $this->conn->prepare
        ('SELECT *
                  FROM events_subjects
                  WHERE events_subjects.parent_id = ?'
        );
        $stmt->bind_param("i", $subject_id);
        $stmt->execute();
        $data = $stmt->get_result();
        $res_arr = array();
        while ($row = $data->fetch_assoc()) {
            $res_arr[] = $row;
        }
        return $res_arr;
    }

}