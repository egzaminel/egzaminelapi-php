<?php

class DB_Functions {

    private $conn;
    private $groups_controller;

    function __construct() {
        require_once 'DB_Connect.php';
        require_once 'DB_Groups.php';

        $db = new DB_connect();
        $this->groups_controller = new DB_Groups();
        $this->conn = $db->connect();
    }

    function __destruct() {
    }

    public function getUserData($user_id) {
        $groups = $this->groups_controller->getAllData($user_id);
        return json_encode($groups);
    }

    public function getGroupData($group_id) {
        $group = $this->groups_controller->getGroupData($group_id);
        return json_encode($group);
    }

}