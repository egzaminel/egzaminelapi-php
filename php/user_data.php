<?php
header('Content-Type: application/json');

require_once 'include/DB_Functions.php';
$db = new DB_Functions();

$error = array("error" => true);
if (isset($_GET['user_id'])) {

    $user_id = $_GET['user_id'];
    $response = $db->getUserData($user_id);

    echo $response;
}

else {
    echo json_encode($error);
}

?>